#!/usr/bin/env bash

#install_jdk
wget https://imcfile.oss-cn-beijing.aliyuncs.com/shizhan/file/liaoshixiong/jdk-8u231-linux-x64.tar.gz
tar zxvf jdk-8u231-linux-x64.tar.gz
rm -rf jdk-8u231-linux-x64.tar.gz
mv jdk1.8.0_231 /usr/local/
ln -s /usr/local/jdk1.8.0_231/bin/java /usr/bin/java
echo -e "JAVA_HOME=/usr/local/jdk1.8.0_231;\nexport PATH=\$PATH:\$JAVA_HOME/bin;" >> /etc/profile
source /etc/profile