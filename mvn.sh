#!/usr/bin/env bash
wget https://mirror.bit.edu.cn/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
tar zxvf apache-maven-3.6.3-bin.tar.gz
rm -rf apache-maven-3.6.3-bin.tar.gz
mv apache-maven-3.6.3 /usr/local/
ln -s /usr/local/apache-maven-3.6.3/bin/maven /usr/bin/maven
echo -e "M3_HOME=/usr/local/apache-maven-3.6.3;\nexport PATH=\$PATH:\$M3_HOME/bin;" >> /etc/profile
source /etc/profile